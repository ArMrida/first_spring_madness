package util;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import com.base.model.TrainingActivity;
import com.base.model.TrainingType;

public class DefaultTraningActivityList {

    private DefaultTraningActivityList() {

    }

    public static List<TrainingActivity> getDafaultTraningActivityList() {
        LocalDateTime startTime1 = LocalDateTime.now();
        LocalDateTime endTime1 = startTime1.plusHours(1);
        TrainingActivity trainingActivity1 = new TrainingActivity(startTime1, endTime1, TrainingType.STRENGTH);

        LocalDateTime startTime2 = endTime1.plusDays(1);
        LocalDateTime endTime2 = startTime2.plusHours(2);
        TrainingActivity trainingActivity2 = new TrainingActivity(startTime2, endTime2, TrainingType.SLOW_LONG_RUNNING);

        LocalDateTime startTime3 = endTime2.plusDays(1).plusHours(2);
        LocalDateTime endTime3 = startTime3.plusHours(1).plusMinutes(20);
        TrainingActivity trainingActivity3 = new TrainingActivity(startTime3, endTime3, TrainingType.STRENGTH);

        return Arrays.asList(trainingActivity1, trainingActivity2, trainingActivity3);
    }
}
