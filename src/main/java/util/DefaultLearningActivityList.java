package util;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import com.base.model.LearningActivity;
import com.base.model.LearningType;
public class DefaultLearningActivityList {
    
    private DefaultLearningActivityList() {}
    
    static List<LearningActivity> getDefaultLearningActivityList() {
        LocalDateTime startTime1 = LocalDateTime.now();
        LocalDateTime endTime1 = startTime1.plusHours(1);
        String comment1="ez az elso comment";
        LearningActivity learningActivity1 = new LearningActivity(startTime1, endTime1, LearningType.LEARN_FROM_BOOK, comment1);
        
        LocalDateTime startTime2= LocalDateTime.now();
        LocalDateTime endTime2 = startTime2.plusHours(1);
        String comment2="ez az masodik comment";
        LearningActivity learningActivity2 = new LearningActivity(startTime2, endTime1, LearningType.LEARN_FROM_VIDEO, comment2);
        
        LocalDateTime startTime3 = LocalDateTime.now();
        LocalDateTime endTime3 = startTime3.plusHours(1);
        String comment3="ez az harmadik comment";
        LearningActivity learningActivity3 = new LearningActivity(startTime3, endTime1, LearningType.LEARN_FROM_BOOK, comment3);
       
        return Arrays.asList(learningActivity1, learningActivity2, learningActivity3);
    
    }
}