package com.base.model;

import java.time.LocalDateTime;
import java.util.Objects;

public class TrainingActivity {
	private final LocalDateTime startTime;
	private final LocalDateTime endTime;
	private final TrainingType trainingType;

	public TrainingActivity(LocalDateTime startTime, LocalDateTime endTime, TrainingType trainingType) {
		super();
		this.startTime = Objects.requireNonNull(startTime, "start time must not ber null") ;
		this.endTime = Objects.requireNonNull(endTime, "end time must not be null");
		this.trainingType = Objects.requireNonNull(trainingType,"training type must not be null");
	}

	@Override
	public String toString() {
		return "TrainingActivity [startTime=" + startTime + ", endTime=" + endTime + ", trainingType=" + trainingType
				+ "]";
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public TrainingType getTrainingType() {
		return trainingType;
	}

}
