package com.base.model;

public enum LearningType {
	PROGRAMMING("programming"), // private static final LearningType PROGRAMMING = new LearningType("programming")
	LEARN_FROM_VIDEO("learn from video"), 
	LEARN_FROM_BOOK("learn from book");
	
	private final String prettyName;

	private LearningType(String prettyName) {
		this.prettyName = prettyName;
	}

	public String getPrettyName() {
		return prettyName;
	}
	public static LearningType findByPrettyName(String key) {
		
		for (LearningType learningType : values()) {
			if (learningType.prettyName.equals(key)) {
				return learningType;
			}
		}
		throw new IllegalArgumentException("Unsupported learning type: " + key);
	}
}