package com.base.model;

import java.time.LocalDateTime;
import java.util.Objects;

public class LearningActivity {
	private final LocalDateTime startTime;
	private final LearningType learningType;
	private final String comment;
	
	public LearningActivity(LocalDateTime startTime, LocalDateTime endTime, LearningType learningType, String comment) {
		super();
		this.startTime = Objects.requireNonNull(startTime, "start time must not be null") ;
		this.endTime = Objects.requireNonNull(endTime, "end time must not be null") ;
		this.learningType = Objects.requireNonNull(learningType, "learning type must not be null") ;
		this.comment = Objects.requireNonNull(comment, "comment must not be null") ;
	}
	public LocalDateTime getStartTime() {
		return startTime;
	}

	public LearningType getLearningType() {
		return learningType;
	}

	public String getComment() {
		return comment;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	private final LocalDateTime endTime;

	
}
