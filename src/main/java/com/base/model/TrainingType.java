package com.base.model;

public enum TrainingType {
	SLOW_LONG_RUNNING("Slow long running"), INTERVAL_TRAINING("Interval training"), STRENGTH("Strength");

	private final String trainingName;

	public String getTrainingName() {
		return trainingName;
	}

	private TrainingType(String trainingName) {
		this.trainingName = trainingName;
	}

	public static TrainingType findByName(String value) {
		for (TrainingType trainingType : values()) {
			if (trainingType.trainingName.equals(value)) {
				return trainingType;
			}
		}
		throw new IllegalArgumentException("Unsupported training type: " + value);
	}

}
